﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace WcfMathApp
{
    [ServiceContract]
    public interface IMathSvc
    {
        [OperationContract]
        [WebInvoke(Method ="GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/add?num1={num1}&num2={num2}")]
        string Add(int num1, int num2);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/specialadd?num1={num1}&num2={num2}")]
        string SpecialAdd(int num1, int num2);
    }
}
