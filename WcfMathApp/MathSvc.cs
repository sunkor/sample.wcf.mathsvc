﻿namespace WcfMathApp
{
    public class MathSvc : IMathSvc
    {
        public string Add(int num1, int num2)
        {
            return (num1 + num2 + 3).ToString();
        }

        public string SpecialAdd(int num1, int num2)
        {
            System.Threading.Thread.Sleep(5000);
            return (num1 + num2 + 3).ToString();
        }
    }
}
